-- DUT - device under test
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A5


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity dut is
    port (
        -- debugging only
        dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
        dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycle (for debugging purpose only)
        dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycle (for debugging purpose only)
        dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycle (for debugging purpose only)
        dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycle (for debugging purpose only)
        dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycle (for debugging purpose only)
        --
        do   : out std_logic_vector( 1 downto 0 ); -- Data Out
        mrk  : out std_logic;                      -- MaRK off 1st data following pattern
        --
        di   : in std_logic_vector ( 1 downto 0 ); -- Data In
        --
        clk  : in std_logic;                       -- CLocK
        nres : in std_logic                        -- Not RESet, low active reset
    );--]port
end entity dut;


architecture arc of dut is

    signal di_ns : std_logic_vector(1 downto 0);
    signal di_cs : std_logic_vector(1 downto 0) := (others => '0');
    signal internal_dip2_s : std_logic_vector (1 downto 0);
    signal internal_dip3_s : std_logic_vector (1 downto 0);
    signal internal_dip4_s : std_logic_vector (1 downto 0);
    signal internal_dip5_s : std_logic_vector (1 downto 0);

    component fsm is
        port (
            clk  : in std_logic;                       -- CLocK
            nres : in std_logic;                        -- Not RESet, low active reset
            di   : in std_logic_vector( 1 downto 0 );  -- Data In
            do   : out std_logic_vector( 1 downto 0 ); -- Data Out
            mrk  : out std_logic                      -- MaRK off 1st data following pattern
        );--]port
    end component fsm;
    for all : fsm use entity work.fsm(moore);
    -- insert architecture you want to test in line above here
    -- mealy / moore


    component dbgShfReg is
        port (
            clk     : in  std_logic;
            nres    : in  std_logic;
            dataIn  : in  std_logic_vector (1 downto 0);
            dataOut : out std_logic_vector (1 downto 0)
        );--]port
    end component dbgShfReg;
    for all : dbgShfReg use entity work.dbgShfReg(arc);


begin
    
    di_ns <= di;
    dip1 <= di_cs;

    sequLogic:
    process (clk) is
    begin
        if (clk = '1' and clk'event) then
            if(nres = '0') then
                di_cs <= (others => '0');
            else
                di_cs <= di_ns;
            end if;
        else
            null;
        end if;
    end process sequLogic;

    
    
    
    fsmi : fsm
        port map(
            clk  => clk,                      
            nres => nres,                       
            di   => di_cs, 
            do   => do,
            mrk  => mrk                     
        );



    dbgShfRegi1 : dbgShfReg
        port map (
            clk => clk,
            nres => nres,
            dataIn => di_cs,
            dataOut => internal_dip2_s
        );
        
    dip2 <= internal_dip2_s;
    
    dbgShfRegi2 : dbgShfReg
        port map (
            clk => clk,
            nres => nres,
            dataIn => internal_dip2_s,
            dataOut => internal_dip3_s
        );
        
    dip3 <= internal_dip3_s;
    
    dbgShfRegi3 : dbgShfReg
        port map (
            clk => clk,
            nres => nres,
            dataIn => internal_dip3_s,
            dataOut => internal_dip4_s
        );
        
    dip4 <= internal_dip4_s;    
        
    dbgShfRegi4 : dbgShfReg
        port map (
            clk => clk,
            nres => nres,
            dataIn => internal_dip4_s,
            dataOut => internal_dip5_s
        );
        
    dip5 <= internal_dip5_s;
        
    dbgShfRegi5 : dbgShfReg
        port map (
            clk => clk,
            nres => nres,
            dataIn => internal_dip5_s,
            dataOut => dip6
        );
        
end architecture arc;