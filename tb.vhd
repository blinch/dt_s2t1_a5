-- test bench for FSM
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A5

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity tb is 
end entity tb;



architecture arc of tb is


    --
    signal clk  : std_logic;
    signal nres : std_logic;
    signal txData : std_logic_vector (1 downto 0);
    signal mrk : std_logic;
    signal do : std_logic_vector (1 downto 0);

    type sr6x2_type is array (0 to 5) of std_logic_vector(1 downto 0);
    
    signal sr6x2_s : sr6x2_type;

    component DUT is
        port (
            -- debugging only
            dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
            dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycle (for debugging purpose only)
            dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycle (for debugging purpose only)
            dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycle (for debugging purpose only)
            dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycle (for debugging purpose only)
            dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycle (for debugging purpose only)
            --
            do   : out std_logic_vector( 1 downto 0 ); -- Data Out
            mrk  : out std_logic;                      -- MaRK off 1st data following pattern
            --
            di   : in std_logic_vector ( 1 downto 0 ); -- Data In
            --
            clk  : in std_logic;                       -- CLocK
            nres : in std_logic                        -- Not RESet, low active reset
        );
    end component DUT;

    for all : DUT use entity work.dut(arc);


    component stimuliGen is
        port (
            txData : out std_logic_vector( 1 downto 0 );   -- TxData send to DUT
            clk    : out std_logic;                        -- CLocK
            nres   : out std_logic                         -- Not RESet ; low active reset
        );
    end component stimuliGen;

    for all : stimuliGen use entity work.sg(beh);


begin
    duti : DUT
        port map(
            dip1 => sr6x2_s(0),
            dip2 => sr6x2_s(1), 
            dip3 => sr6x2_s(2),
            dip4 => sr6x2_s(3),
            dip5 => sr6x2_s(4), 
            dip6 => sr6x2_s(5),
            do => do,
            mrk => mrk,
            di => txData,
            clk => clk,
            nres => nres
        );
        
    sgi : stimuliGen
        port map(
            txData => txData,
            clk    => clk,
            nres   => nres
        );

end architecture arc;