-- FSM - mealy architecture
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A5


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

architecture mealy of fsm is

    signal z_cs:  std_logic_vector (2 downto 0) := "000";
    signal z_ns:  std_logic_vector (2 downto 0);
	signal di_cs:  std_logic_vector (1 downto 0) := "00";
    signal di_ns:  std_logic_vector (1 downto 0);
	signal do_cs:  std_logic_vector (1 downto 0) := "00";
    signal do_ns:  std_logic_vector (1 downto 0);
	signal mrkdelay_s: std_logic;
	signal mrk_cs: std_logic := '0';
	signal mrk_ns: std_logic := '0';
	
	
begin

combiLogic:
    process (di_cs,z_cs) is 
        variable z_v: std_logic_vector(2 downto 0);
        variable di_v: std_logic_vector(1 downto 0);
        variable mrk_v : std_logic;
    begin
        z_v := z_cs;
        di_v := di_cs;
        mrk_v := '0';
        
        case z_cs is
            
            when "000" =>       -- state 1
                if (di_v = "11") then
                    z_v := "001";
                else
                    z_v := "000";
                end if;          
            
            
            when "001" =>       -- state 2
                if (di_v = "11") then
                    z_v := "010";
                else
                    z_v := "000";
                end if;
            
            
            when "010" =>       -- state 3            
				if (di_v = "00") then
					z_v := "011";
				elsif (di_v = "11") then
					z_v := "010";
				else
					z_v := "000";
				end if;
            
            
            when "011" =>       -- state 4
				if (di_v = "00") then
					z_v := "000";
					mrk_v := '1';
				elsif (di_v = "11") then
					z_v := "001";
				else
					z_v := "000";
				end if;
            
            when others =>
                z_v := "000";
        end case;
        
        z_ns <= z_v;
        do_ns <= di_v;
        mrkdelay_s <= mrk_v;        

    
    end process combiLogic;

sequLogic:
    process (clk) is
    begin
        if(clk = '1' and clk'event) then
            if(nres = '0') then
                z_cs <= (others => '0');
				mrk_cs <= '0';
				mrkdelay_s <= '0';
				do_cs <= (others => '0');
				di_cs <= (others => '0');
            else
               z_cs <= z_ns;
			   mrk_ns <= mrkdelay_s;
			   mrk_cs <= mrk_ns;
			   do_cs <= do_ns;
			   di_cs <= di_ns;
            end if;
        else
            null;
        end if;
    end process sequLogic;

	di_ns <= di;
	do  <= do_cs;
	mrk <= mrk_cs;

end architecture mealy;