--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: dut_timesim.vhd
-- /___/   /\     Timestamp: Fri Jan 15 13:03:53 2016
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm dut -w -dir netgen/fit -ofmt vhdl -sim dut.nga dut_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: dut.nga
-- Output file	: D:\DTP\A5\ise14x7_v1\iseWRK\netgen\fit\dut_timesim.vhd
-- # of Entities	: 1
-- Design Name	: dut.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity dut is
  port (
    clk : in STD_LOGIC := 'X'; 
    nres : in STD_LOGIC := 'X'; 
    mrk : out STD_LOGIC; 
    di : in STD_LOGIC_VECTOR ( 1 downto 0 ); 
    dip1 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    dip2 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    dip3 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    dip4 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    dip5 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    dip6 : out STD_LOGIC_VECTOR ( 1 downto 0 ); 
    do : out STD_LOGIC_VECTOR ( 1 downto 0 ) 
  );
end dut;

architecture Structure of dut is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal di_0_II_UIM_3 : STD_LOGIC; 
  signal di_1_II_UIM_5 : STD_LOGIC; 
  signal nres_II_UIM_7 : STD_LOGIC; 
  signal dip1_0_MC_Q_9 : STD_LOGIC; 
  signal dip1_1_MC_Q_11 : STD_LOGIC; 
  signal dip2_0_MC_Q_13 : STD_LOGIC; 
  signal dip2_1_MC_Q_15 : STD_LOGIC; 
  signal dip3_0_MC_Q_17 : STD_LOGIC; 
  signal dip3_1_MC_Q_19 : STD_LOGIC; 
  signal dip4_0_MC_Q_21 : STD_LOGIC; 
  signal dip4_1_MC_Q_23 : STD_LOGIC; 
  signal dip5_0_MC_Q_25 : STD_LOGIC; 
  signal dip5_1_MC_Q_27 : STD_LOGIC; 
  signal dip6_0_MC_Q_29 : STD_LOGIC; 
  signal dip6_1_MC_Q_31 : STD_LOGIC; 
  signal do_0_MC_Q_33 : STD_LOGIC; 
  signal do_1_MC_Q_35 : STD_LOGIC; 
  signal mrk_MC_Q_37 : STD_LOGIC; 
  signal dip1_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip1_0_MC_D_39 : STD_LOGIC; 
  signal Gnd_40 : STD_LOGIC; 
  signal Vcc_41 : STD_LOGIC; 
  signal dip1_0_MC_D1_42 : STD_LOGIC; 
  signal dip1_0_MC_D2_43 : STD_LOGIC; 
  signal dip1_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip1_1_MC_D_45 : STD_LOGIC; 
  signal dip1_1_MC_D1_46 : STD_LOGIC; 
  signal dip1_1_MC_D2_47 : STD_LOGIC; 
  signal dip2_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip2_0_MC_UIM_49 : STD_LOGIC; 
  signal dip2_0_MC_D_50 : STD_LOGIC; 
  signal dip2_0_MC_D1_51 : STD_LOGIC; 
  signal dip2_0_MC_D2_52 : STD_LOGIC; 
  signal do_0_MC_UIM_53 : STD_LOGIC; 
  signal do_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal do_0_MC_D_55 : STD_LOGIC; 
  signal do_0_MC_D1_56 : STD_LOGIC; 
  signal do_0_MC_D2_57 : STD_LOGIC; 
  signal dip2_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip2_1_MC_UIM_59 : STD_LOGIC; 
  signal dip2_1_MC_D_60 : STD_LOGIC; 
  signal dip2_1_MC_D1_61 : STD_LOGIC; 
  signal dip2_1_MC_D2_62 : STD_LOGIC; 
  signal do_1_MC_UIM_63 : STD_LOGIC; 
  signal do_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal do_1_MC_D_65 : STD_LOGIC; 
  signal do_1_MC_D1_66 : STD_LOGIC; 
  signal do_1_MC_D2_67 : STD_LOGIC; 
  signal dip3_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip3_0_MC_UIM_69 : STD_LOGIC; 
  signal dip3_0_MC_D_70 : STD_LOGIC; 
  signal dip3_0_MC_D1_71 : STD_LOGIC; 
  signal dip3_0_MC_D2_72 : STD_LOGIC; 
  signal dip3_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip3_1_MC_UIM_74 : STD_LOGIC; 
  signal dip3_1_MC_D_75 : STD_LOGIC; 
  signal dip3_1_MC_D1_76 : STD_LOGIC; 
  signal dip3_1_MC_D2_77 : STD_LOGIC; 
  signal dip4_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip4_0_MC_UIM_79 : STD_LOGIC; 
  signal dip4_0_MC_D_80 : STD_LOGIC; 
  signal dip4_0_MC_D1_81 : STD_LOGIC; 
  signal dip4_0_MC_D2_82 : STD_LOGIC; 
  signal dip4_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip4_1_MC_UIM_84 : STD_LOGIC; 
  signal dip4_1_MC_D_85 : STD_LOGIC; 
  signal dip4_1_MC_D1_86 : STD_LOGIC; 
  signal dip4_1_MC_D2_87 : STD_LOGIC; 
  signal dip5_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip5_0_MC_UIM_89 : STD_LOGIC; 
  signal dip5_0_MC_D_90 : STD_LOGIC; 
  signal dip5_0_MC_D1_91 : STD_LOGIC; 
  signal dip5_0_MC_D2_92 : STD_LOGIC; 
  signal dip5_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip5_1_MC_UIM_94 : STD_LOGIC; 
  signal dip5_1_MC_D_95 : STD_LOGIC; 
  signal dip5_1_MC_D1_96 : STD_LOGIC; 
  signal dip5_1_MC_D2_97 : STD_LOGIC; 
  signal dip6_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip6_0_MC_D_99 : STD_LOGIC; 
  signal dip6_0_MC_D1_100 : STD_LOGIC; 
  signal dip6_0_MC_D2_101 : STD_LOGIC; 
  signal dip6_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal dip6_1_MC_D_103 : STD_LOGIC; 
  signal dip6_1_MC_D1_104 : STD_LOGIC; 
  signal dip6_1_MC_D2_105 : STD_LOGIC; 
  signal mrk_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal mrk_MC_UIM_107 : STD_LOGIC; 
  signal mrk_MC_D_108 : STD_LOGIC; 
  signal mrk_MC_D1_109 : STD_LOGIC; 
  signal mrk_MC_D2_110 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd3_111 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_112 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd3_MC_Q : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd3_MC_D_114 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd3_MC_D1_115 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd3_MC_D2_116 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_MC_Q : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_MC_D_118 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_MC_D1_119 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_MC_D2_120 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_121 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_122 : STD_LOGIC; 
  signal fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_123 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip1_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip2_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_do_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip3_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip4_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip5_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_dip6_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_mrk_MC_D1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_mrk_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_mrk_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_mrk_MC_D1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_mrk_MC_D1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2 : STD_LOGIC; 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  di_0_II_UIM : X_BUF
    port map (
      I => di(0),
      O => di_0_II_UIM_3
    );
  di_1_II_UIM : X_BUF
    port map (
      I => di(1),
      O => di_1_II_UIM_5
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_7
    );
  dip1_0_Q : X_BUF
    port map (
      I => dip1_0_MC_Q_9,
      O => dip1(0)
    );
  dip1_1_Q : X_BUF
    port map (
      I => dip1_1_MC_Q_11,
      O => dip1(1)
    );
  dip2_0_Q : X_BUF
    port map (
      I => dip2_0_MC_Q_13,
      O => dip2(0)
    );
  dip2_1_Q : X_BUF
    port map (
      I => dip2_1_MC_Q_15,
      O => dip2(1)
    );
  dip3_0_Q : X_BUF
    port map (
      I => dip3_0_MC_Q_17,
      O => dip3(0)
    );
  dip3_1_Q : X_BUF
    port map (
      I => dip3_1_MC_Q_19,
      O => dip3(1)
    );
  dip4_0_Q : X_BUF
    port map (
      I => dip4_0_MC_Q_21,
      O => dip4(0)
    );
  dip4_1_Q : X_BUF
    port map (
      I => dip4_1_MC_Q_23,
      O => dip4(1)
    );
  dip5_0_Q : X_BUF
    port map (
      I => dip5_0_MC_Q_25,
      O => dip5(0)
    );
  dip5_1_Q : X_BUF
    port map (
      I => dip5_1_MC_Q_27,
      O => dip5(1)
    );
  dip6_0_Q : X_BUF
    port map (
      I => dip6_0_MC_Q_29,
      O => dip6(0)
    );
  dip6_1_Q : X_BUF
    port map (
      I => dip6_1_MC_Q_31,
      O => dip6(1)
    );
  do_0_Q : X_BUF
    port map (
      I => do_0_MC_Q_33,
      O => do(0)
    );
  do_1_Q : X_BUF
    port map (
      I => do_1_MC_Q_35,
      O => do(1)
    );
  mrk_38 : X_BUF
    port map (
      I => mrk_MC_Q_37,
      O => mrk
    );
  dip1_0_MC_Q : X_BUF
    port map (
      I => dip1_0_MC_Q_tsimrenamed_net_Q,
      O => dip1_0_MC_Q_9
    );
  dip1_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip1_0_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip1_0_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip1_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_40
    );
  Vcc : X_ONE
    port map (
      O => Vcc_41
    );
  dip1_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip1_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip1_0_MC_D_IN1,
      O => dip1_0_MC_D_39
    );
  dip1_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip1_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip1_0_MC_D1_IN1,
      O => dip1_0_MC_D1_42
    );
  dip1_0_MC_D2 : X_ZERO
    port map (
      O => dip1_0_MC_D2_43
    );
  dip1_1_MC_Q : X_BUF
    port map (
      I => dip1_1_MC_Q_tsimrenamed_net_Q,
      O => dip1_1_MC_Q_11
    );
  dip1_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip1_1_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip1_1_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip1_1_MC_Q_tsimrenamed_net_Q
    );
  dip1_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip1_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip1_1_MC_D_IN1,
      O => dip1_1_MC_D_45
    );
  dip1_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip1_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip1_1_MC_D1_IN1,
      O => dip1_1_MC_D1_46
    );
  dip1_1_MC_D2 : X_ZERO
    port map (
      O => dip1_1_MC_D2_47
    );
  dip2_0_MC_Q : X_BUF
    port map (
      I => dip2_0_MC_Q_tsimrenamed_net_Q,
      O => dip2_0_MC_Q_13
    );
  dip2_0_MC_UIM : X_BUF
    port map (
      I => dip2_0_MC_Q_tsimrenamed_net_Q,
      O => dip2_0_MC_UIM_49
    );
  dip2_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip2_0_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip2_0_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip2_0_MC_Q_tsimrenamed_net_Q
    );
  dip2_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip2_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip2_0_MC_D_IN1,
      O => dip2_0_MC_D_50
    );
  dip2_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip2_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip2_0_MC_D1_IN1,
      O => dip2_0_MC_D1_51
    );
  dip2_0_MC_D2 : X_ZERO
    port map (
      O => dip2_0_MC_D2_52
    );
  do_0_MC_Q : X_BUF
    port map (
      I => do_0_MC_Q_tsimrenamed_net_Q,
      O => do_0_MC_Q_33
    );
  do_0_MC_UIM : X_BUF
    port map (
      I => do_0_MC_Q_tsimrenamed_net_Q,
      O => do_0_MC_UIM_53
    );
  do_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_do_0_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_do_0_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => do_0_MC_Q_tsimrenamed_net_Q
    );
  do_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_do_0_MC_D_IN0,
      I1 => NlwBufferSignal_do_0_MC_D_IN1,
      O => do_0_MC_D_55
    );
  do_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_do_0_MC_D1_IN0,
      I1 => NlwBufferSignal_do_0_MC_D1_IN1,
      O => do_0_MC_D1_56
    );
  do_0_MC_D2 : X_ZERO
    port map (
      O => do_0_MC_D2_57
    );
  dip2_1_MC_Q : X_BUF
    port map (
      I => dip2_1_MC_Q_tsimrenamed_net_Q,
      O => dip2_1_MC_Q_15
    );
  dip2_1_MC_UIM : X_BUF
    port map (
      I => dip2_1_MC_Q_tsimrenamed_net_Q,
      O => dip2_1_MC_UIM_59
    );
  dip2_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip2_1_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip2_1_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip2_1_MC_Q_tsimrenamed_net_Q
    );
  dip2_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip2_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip2_1_MC_D_IN1,
      O => dip2_1_MC_D_60
    );
  dip2_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip2_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip2_1_MC_D1_IN1,
      O => dip2_1_MC_D1_61
    );
  dip2_1_MC_D2 : X_ZERO
    port map (
      O => dip2_1_MC_D2_62
    );
  do_1_MC_Q : X_BUF
    port map (
      I => do_1_MC_Q_tsimrenamed_net_Q,
      O => do_1_MC_Q_35
    );
  do_1_MC_UIM : X_BUF
    port map (
      I => do_1_MC_Q_tsimrenamed_net_Q,
      O => do_1_MC_UIM_63
    );
  do_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_do_1_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_do_1_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => do_1_MC_Q_tsimrenamed_net_Q
    );
  do_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_do_1_MC_D_IN0,
      I1 => NlwBufferSignal_do_1_MC_D_IN1,
      O => do_1_MC_D_65
    );
  do_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_do_1_MC_D1_IN0,
      I1 => NlwBufferSignal_do_1_MC_D1_IN1,
      O => do_1_MC_D1_66
    );
  do_1_MC_D2 : X_ZERO
    port map (
      O => do_1_MC_D2_67
    );
  dip3_0_MC_Q : X_BUF
    port map (
      I => dip3_0_MC_Q_tsimrenamed_net_Q,
      O => dip3_0_MC_Q_17
    );
  dip3_0_MC_UIM : X_BUF
    port map (
      I => dip3_0_MC_Q_tsimrenamed_net_Q,
      O => dip3_0_MC_UIM_69
    );
  dip3_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip3_0_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip3_0_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip3_0_MC_Q_tsimrenamed_net_Q
    );
  dip3_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip3_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip3_0_MC_D_IN1,
      O => dip3_0_MC_D_70
    );
  dip3_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip3_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip3_0_MC_D1_IN1,
      O => dip3_0_MC_D1_71
    );
  dip3_0_MC_D2 : X_ZERO
    port map (
      O => dip3_0_MC_D2_72
    );
  dip3_1_MC_Q : X_BUF
    port map (
      I => dip3_1_MC_Q_tsimrenamed_net_Q,
      O => dip3_1_MC_Q_19
    );
  dip3_1_MC_UIM : X_BUF
    port map (
      I => dip3_1_MC_Q_tsimrenamed_net_Q,
      O => dip3_1_MC_UIM_74
    );
  dip3_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip3_1_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip3_1_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip3_1_MC_Q_tsimrenamed_net_Q
    );
  dip3_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip3_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip3_1_MC_D_IN1,
      O => dip3_1_MC_D_75
    );
  dip3_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip3_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip3_1_MC_D1_IN1,
      O => dip3_1_MC_D1_76
    );
  dip3_1_MC_D2 : X_ZERO
    port map (
      O => dip3_1_MC_D2_77
    );
  dip4_0_MC_Q : X_BUF
    port map (
      I => dip4_0_MC_Q_tsimrenamed_net_Q,
      O => dip4_0_MC_Q_21
    );
  dip4_0_MC_UIM : X_BUF
    port map (
      I => dip4_0_MC_Q_tsimrenamed_net_Q,
      O => dip4_0_MC_UIM_79
    );
  dip4_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip4_0_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip4_0_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip4_0_MC_Q_tsimrenamed_net_Q
    );
  dip4_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip4_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip4_0_MC_D_IN1,
      O => dip4_0_MC_D_80
    );
  dip4_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip4_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip4_0_MC_D1_IN1,
      O => dip4_0_MC_D1_81
    );
  dip4_0_MC_D2 : X_ZERO
    port map (
      O => dip4_0_MC_D2_82
    );
  dip4_1_MC_Q : X_BUF
    port map (
      I => dip4_1_MC_Q_tsimrenamed_net_Q,
      O => dip4_1_MC_Q_23
    );
  dip4_1_MC_UIM : X_BUF
    port map (
      I => dip4_1_MC_Q_tsimrenamed_net_Q,
      O => dip4_1_MC_UIM_84
    );
  dip4_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip4_1_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip4_1_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip4_1_MC_Q_tsimrenamed_net_Q
    );
  dip4_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip4_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip4_1_MC_D_IN1,
      O => dip4_1_MC_D_85
    );
  dip4_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip4_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip4_1_MC_D1_IN1,
      O => dip4_1_MC_D1_86
    );
  dip4_1_MC_D2 : X_ZERO
    port map (
      O => dip4_1_MC_D2_87
    );
  dip5_0_MC_Q : X_BUF
    port map (
      I => dip5_0_MC_Q_tsimrenamed_net_Q,
      O => dip5_0_MC_Q_25
    );
  dip5_0_MC_UIM : X_BUF
    port map (
      I => dip5_0_MC_Q_tsimrenamed_net_Q,
      O => dip5_0_MC_UIM_89
    );
  dip5_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip5_0_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip5_0_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip5_0_MC_Q_tsimrenamed_net_Q
    );
  dip5_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip5_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip5_0_MC_D_IN1,
      O => dip5_0_MC_D_90
    );
  dip5_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip5_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip5_0_MC_D1_IN1,
      O => dip5_0_MC_D1_91
    );
  dip5_0_MC_D2 : X_ZERO
    port map (
      O => dip5_0_MC_D2_92
    );
  dip5_1_MC_Q : X_BUF
    port map (
      I => dip5_1_MC_Q_tsimrenamed_net_Q,
      O => dip5_1_MC_Q_27
    );
  dip5_1_MC_UIM : X_BUF
    port map (
      I => dip5_1_MC_Q_tsimrenamed_net_Q,
      O => dip5_1_MC_UIM_94
    );
  dip5_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip5_1_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip5_1_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip5_1_MC_Q_tsimrenamed_net_Q
    );
  dip5_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip5_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip5_1_MC_D_IN1,
      O => dip5_1_MC_D_95
    );
  dip5_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip5_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip5_1_MC_D1_IN1,
      O => dip5_1_MC_D1_96
    );
  dip5_1_MC_D2 : X_ZERO
    port map (
      O => dip5_1_MC_D2_97
    );
  dip6_0_MC_Q : X_BUF
    port map (
      I => dip6_0_MC_Q_tsimrenamed_net_Q,
      O => dip6_0_MC_Q_29
    );
  dip6_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip6_0_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip6_0_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip6_0_MC_Q_tsimrenamed_net_Q
    );
  dip6_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip6_0_MC_D_IN0,
      I1 => NlwBufferSignal_dip6_0_MC_D_IN1,
      O => dip6_0_MC_D_99
    );
  dip6_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip6_0_MC_D1_IN0,
      I1 => NlwBufferSignal_dip6_0_MC_D1_IN1,
      O => dip6_0_MC_D1_100
    );
  dip6_0_MC_D2 : X_ZERO
    port map (
      O => dip6_0_MC_D2_101
    );
  dip6_1_MC_Q : X_BUF
    port map (
      I => dip6_1_MC_Q_tsimrenamed_net_Q,
      O => dip6_1_MC_Q_31
    );
  dip6_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_dip6_1_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_dip6_1_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => dip6_1_MC_Q_tsimrenamed_net_Q
    );
  dip6_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_dip6_1_MC_D_IN0,
      I1 => NlwBufferSignal_dip6_1_MC_D_IN1,
      O => dip6_1_MC_D_103
    );
  dip6_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_dip6_1_MC_D1_IN0,
      I1 => NlwBufferSignal_dip6_1_MC_D1_IN1,
      O => dip6_1_MC_D1_104
    );
  dip6_1_MC_D2 : X_ZERO
    port map (
      O => dip6_1_MC_D2_105
    );
  mrk_MC_Q : X_BUF
    port map (
      I => mrk_MC_Q_tsimrenamed_net_Q,
      O => mrk_MC_Q_37
    );
  mrk_MC_UIM : X_BUF
    port map (
      I => mrk_MC_Q_tsimrenamed_net_Q,
      O => mrk_MC_UIM_107
    );
  mrk_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_mrk_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_mrk_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => mrk_MC_Q_tsimrenamed_net_Q
    );
  mrk_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_mrk_MC_D_IN0,
      I1 => NlwBufferSignal_mrk_MC_D_IN1,
      O => mrk_MC_D_108
    );
  mrk_MC_D1 : X_AND6
    port map (
      I0 => NlwBufferSignal_mrk_MC_D1_IN0,
      I1 => NlwInverterSignal_mrk_MC_D1_IN1,
      I2 => NlwInverterSignal_mrk_MC_D1_IN2,
      I3 => NlwInverterSignal_mrk_MC_D1_IN3,
      I4 => NlwInverterSignal_mrk_MC_D1_IN4,
      I5 => NlwBufferSignal_mrk_MC_D1_IN5,
      O => mrk_MC_D1_109
    );
  mrk_MC_D2 : X_ZERO
    port map (
      O => mrk_MC_D2_110
    );
  fsmi_z_cs_FSM_FFd3 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd3_MC_Q,
      O => fsmi_z_cs_FSM_FFd3_111
    );
  fsmi_z_cs_FSM_FFd3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => fsmi_z_cs_FSM_FFd3_MC_Q
    );
  fsmi_z_cs_FSM_FFd3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D_IN0,
      I1 => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D_IN1,
      O => fsmi_z_cs_FSM_FFd3_MC_D_114
    );
  fsmi_z_cs_FSM_FFd3_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN0,
      I1 => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN1,
      I2 => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN2,
      O => fsmi_z_cs_FSM_FFd3_MC_D1_115
    );
  fsmi_z_cs_FSM_FFd3_MC_D2 : X_ZERO
    port map (
      O => fsmi_z_cs_FSM_FFd3_MC_D2_116
    );
  fsmi_z_cs_FSM_FFd2 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_MC_Q,
      O => fsmi_z_cs_FSM_FFd2_112
    );
  fsmi_z_cs_FSM_FFd2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_REG_IN,
      CE => Vcc_41,
      CLK => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_REG_CLK,
      SET => Gnd_40,
      RST => Gnd_40,
      O => fsmi_z_cs_FSM_FFd2_MC_Q
    );
  fsmi_z_cs_FSM_FFd2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D_IN0,
      I1 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D_IN1,
      O => fsmi_z_cs_FSM_FFd2_MC_D_118
    );
  fsmi_z_cs_FSM_FFd2_MC_D1 : X_ZERO
    port map (
      O => fsmi_z_cs_FSM_FFd2_MC_D1_119
    );
  fsmi_z_cs_FSM_FFd2_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN3,
      O => fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_121
    );
  fsmi_z_cs_FSM_FFd2_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN4,
      O => fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_122
    );
  fsmi_z_cs_FSM_FFd2_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN4,
      O => fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_123
    );
  fsmi_z_cs_FSM_FFd2_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN0,
      I1 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN1,
      I2 => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN2,
      O => fsmi_z_cs_FSM_FFd2_MC_D2_120
    );
  NlwBufferBlock_dip1_0_MC_REG_IN : X_BUF
    port map (
      I => dip1_0_MC_D_39,
      O => NlwBufferSignal_dip1_0_MC_REG_IN
    );
  NlwBufferBlock_dip1_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip1_0_MC_REG_CLK
    );
  NlwBufferBlock_dip1_0_MC_D_IN0 : X_BUF
    port map (
      I => dip1_0_MC_D1_42,
      O => NlwBufferSignal_dip1_0_MC_D_IN0
    );
  NlwBufferBlock_dip1_0_MC_D_IN1 : X_BUF
    port map (
      I => dip1_0_MC_D2_43,
      O => NlwBufferSignal_dip1_0_MC_D_IN1
    );
  NlwBufferBlock_dip1_0_MC_D1_IN0 : X_BUF
    port map (
      I => di_0_II_UIM_3,
      O => NlwBufferSignal_dip1_0_MC_D1_IN0
    );
  NlwBufferBlock_dip1_0_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip1_0_MC_D1_IN1
    );
  NlwBufferBlock_dip1_1_MC_REG_IN : X_BUF
    port map (
      I => dip1_1_MC_D_45,
      O => NlwBufferSignal_dip1_1_MC_REG_IN
    );
  NlwBufferBlock_dip1_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip1_1_MC_REG_CLK
    );
  NlwBufferBlock_dip1_1_MC_D_IN0 : X_BUF
    port map (
      I => dip1_1_MC_D1_46,
      O => NlwBufferSignal_dip1_1_MC_D_IN0
    );
  NlwBufferBlock_dip1_1_MC_D_IN1 : X_BUF
    port map (
      I => dip1_1_MC_D2_47,
      O => NlwBufferSignal_dip1_1_MC_D_IN1
    );
  NlwBufferBlock_dip1_1_MC_D1_IN0 : X_BUF
    port map (
      I => di_1_II_UIM_5,
      O => NlwBufferSignal_dip1_1_MC_D1_IN0
    );
  NlwBufferBlock_dip1_1_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip1_1_MC_D1_IN1
    );
  NlwBufferBlock_dip2_0_MC_REG_IN : X_BUF
    port map (
      I => dip2_0_MC_D_50,
      O => NlwBufferSignal_dip2_0_MC_REG_IN
    );
  NlwBufferBlock_dip2_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip2_0_MC_REG_CLK
    );
  NlwBufferBlock_dip2_0_MC_D_IN0 : X_BUF
    port map (
      I => dip2_0_MC_D1_51,
      O => NlwBufferSignal_dip2_0_MC_D_IN0
    );
  NlwBufferBlock_dip2_0_MC_D_IN1 : X_BUF
    port map (
      I => dip2_0_MC_D2_52,
      O => NlwBufferSignal_dip2_0_MC_D_IN1
    );
  NlwBufferBlock_dip2_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip2_0_MC_D1_IN0
    );
  NlwBufferBlock_dip2_0_MC_D1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_53,
      O => NlwBufferSignal_dip2_0_MC_D1_IN1
    );
  NlwBufferBlock_do_0_MC_REG_IN : X_BUF
    port map (
      I => do_0_MC_D_55,
      O => NlwBufferSignal_do_0_MC_REG_IN
    );
  NlwBufferBlock_do_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_do_0_MC_REG_CLK
    );
  NlwBufferBlock_do_0_MC_D_IN0 : X_BUF
    port map (
      I => do_0_MC_D1_56,
      O => NlwBufferSignal_do_0_MC_D_IN0
    );
  NlwBufferBlock_do_0_MC_D_IN1 : X_BUF
    port map (
      I => do_0_MC_D2_57,
      O => NlwBufferSignal_do_0_MC_D_IN1
    );
  NlwBufferBlock_do_0_MC_D1_IN0 : X_BUF
    port map (
      I => di_0_II_UIM_3,
      O => NlwBufferSignal_do_0_MC_D1_IN0
    );
  NlwBufferBlock_do_0_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_do_0_MC_D1_IN1
    );
  NlwBufferBlock_dip2_1_MC_REG_IN : X_BUF
    port map (
      I => dip2_1_MC_D_60,
      O => NlwBufferSignal_dip2_1_MC_REG_IN
    );
  NlwBufferBlock_dip2_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip2_1_MC_REG_CLK
    );
  NlwBufferBlock_dip2_1_MC_D_IN0 : X_BUF
    port map (
      I => dip2_1_MC_D1_61,
      O => NlwBufferSignal_dip2_1_MC_D_IN0
    );
  NlwBufferBlock_dip2_1_MC_D_IN1 : X_BUF
    port map (
      I => dip2_1_MC_D2_62,
      O => NlwBufferSignal_dip2_1_MC_D_IN1
    );
  NlwBufferBlock_dip2_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip2_1_MC_D1_IN0
    );
  NlwBufferBlock_dip2_1_MC_D1_IN1 : X_BUF
    port map (
      I => do_1_MC_UIM_63,
      O => NlwBufferSignal_dip2_1_MC_D1_IN1
    );
  NlwBufferBlock_do_1_MC_REG_IN : X_BUF
    port map (
      I => do_1_MC_D_65,
      O => NlwBufferSignal_do_1_MC_REG_IN
    );
  NlwBufferBlock_do_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_do_1_MC_REG_CLK
    );
  NlwBufferBlock_do_1_MC_D_IN0 : X_BUF
    port map (
      I => do_1_MC_D1_66,
      O => NlwBufferSignal_do_1_MC_D_IN0
    );
  NlwBufferBlock_do_1_MC_D_IN1 : X_BUF
    port map (
      I => do_1_MC_D2_67,
      O => NlwBufferSignal_do_1_MC_D_IN1
    );
  NlwBufferBlock_do_1_MC_D1_IN0 : X_BUF
    port map (
      I => di_1_II_UIM_5,
      O => NlwBufferSignal_do_1_MC_D1_IN0
    );
  NlwBufferBlock_do_1_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_do_1_MC_D1_IN1
    );
  NlwBufferBlock_dip3_0_MC_REG_IN : X_BUF
    port map (
      I => dip3_0_MC_D_70,
      O => NlwBufferSignal_dip3_0_MC_REG_IN
    );
  NlwBufferBlock_dip3_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip3_0_MC_REG_CLK
    );
  NlwBufferBlock_dip3_0_MC_D_IN0 : X_BUF
    port map (
      I => dip3_0_MC_D1_71,
      O => NlwBufferSignal_dip3_0_MC_D_IN0
    );
  NlwBufferBlock_dip3_0_MC_D_IN1 : X_BUF
    port map (
      I => dip3_0_MC_D2_72,
      O => NlwBufferSignal_dip3_0_MC_D_IN1
    );
  NlwBufferBlock_dip3_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip3_0_MC_D1_IN0
    );
  NlwBufferBlock_dip3_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip2_0_MC_UIM_49,
      O => NlwBufferSignal_dip3_0_MC_D1_IN1
    );
  NlwBufferBlock_dip3_1_MC_REG_IN : X_BUF
    port map (
      I => dip3_1_MC_D_75,
      O => NlwBufferSignal_dip3_1_MC_REG_IN
    );
  NlwBufferBlock_dip3_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip3_1_MC_REG_CLK
    );
  NlwBufferBlock_dip3_1_MC_D_IN0 : X_BUF
    port map (
      I => dip3_1_MC_D1_76,
      O => NlwBufferSignal_dip3_1_MC_D_IN0
    );
  NlwBufferBlock_dip3_1_MC_D_IN1 : X_BUF
    port map (
      I => dip3_1_MC_D2_77,
      O => NlwBufferSignal_dip3_1_MC_D_IN1
    );
  NlwBufferBlock_dip3_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip3_1_MC_D1_IN0
    );
  NlwBufferBlock_dip3_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip2_1_MC_UIM_59,
      O => NlwBufferSignal_dip3_1_MC_D1_IN1
    );
  NlwBufferBlock_dip4_0_MC_REG_IN : X_BUF
    port map (
      I => dip4_0_MC_D_80,
      O => NlwBufferSignal_dip4_0_MC_REG_IN
    );
  NlwBufferBlock_dip4_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip4_0_MC_REG_CLK
    );
  NlwBufferBlock_dip4_0_MC_D_IN0 : X_BUF
    port map (
      I => dip4_0_MC_D1_81,
      O => NlwBufferSignal_dip4_0_MC_D_IN0
    );
  NlwBufferBlock_dip4_0_MC_D_IN1 : X_BUF
    port map (
      I => dip4_0_MC_D2_82,
      O => NlwBufferSignal_dip4_0_MC_D_IN1
    );
  NlwBufferBlock_dip4_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip4_0_MC_D1_IN0
    );
  NlwBufferBlock_dip4_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip3_0_MC_UIM_69,
      O => NlwBufferSignal_dip4_0_MC_D1_IN1
    );
  NlwBufferBlock_dip4_1_MC_REG_IN : X_BUF
    port map (
      I => dip4_1_MC_D_85,
      O => NlwBufferSignal_dip4_1_MC_REG_IN
    );
  NlwBufferBlock_dip4_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip4_1_MC_REG_CLK
    );
  NlwBufferBlock_dip4_1_MC_D_IN0 : X_BUF
    port map (
      I => dip4_1_MC_D1_86,
      O => NlwBufferSignal_dip4_1_MC_D_IN0
    );
  NlwBufferBlock_dip4_1_MC_D_IN1 : X_BUF
    port map (
      I => dip4_1_MC_D2_87,
      O => NlwBufferSignal_dip4_1_MC_D_IN1
    );
  NlwBufferBlock_dip4_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip4_1_MC_D1_IN0
    );
  NlwBufferBlock_dip4_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip3_1_MC_UIM_74,
      O => NlwBufferSignal_dip4_1_MC_D1_IN1
    );
  NlwBufferBlock_dip5_0_MC_REG_IN : X_BUF
    port map (
      I => dip5_0_MC_D_90,
      O => NlwBufferSignal_dip5_0_MC_REG_IN
    );
  NlwBufferBlock_dip5_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip5_0_MC_REG_CLK
    );
  NlwBufferBlock_dip5_0_MC_D_IN0 : X_BUF
    port map (
      I => dip5_0_MC_D1_91,
      O => NlwBufferSignal_dip5_0_MC_D_IN0
    );
  NlwBufferBlock_dip5_0_MC_D_IN1 : X_BUF
    port map (
      I => dip5_0_MC_D2_92,
      O => NlwBufferSignal_dip5_0_MC_D_IN1
    );
  NlwBufferBlock_dip5_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip5_0_MC_D1_IN0
    );
  NlwBufferBlock_dip5_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip4_0_MC_UIM_79,
      O => NlwBufferSignal_dip5_0_MC_D1_IN1
    );
  NlwBufferBlock_dip5_1_MC_REG_IN : X_BUF
    port map (
      I => dip5_1_MC_D_95,
      O => NlwBufferSignal_dip5_1_MC_REG_IN
    );
  NlwBufferBlock_dip5_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip5_1_MC_REG_CLK
    );
  NlwBufferBlock_dip5_1_MC_D_IN0 : X_BUF
    port map (
      I => dip5_1_MC_D1_96,
      O => NlwBufferSignal_dip5_1_MC_D_IN0
    );
  NlwBufferBlock_dip5_1_MC_D_IN1 : X_BUF
    port map (
      I => dip5_1_MC_D2_97,
      O => NlwBufferSignal_dip5_1_MC_D_IN1
    );
  NlwBufferBlock_dip5_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip5_1_MC_D1_IN0
    );
  NlwBufferBlock_dip5_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip4_1_MC_UIM_84,
      O => NlwBufferSignal_dip5_1_MC_D1_IN1
    );
  NlwBufferBlock_dip6_0_MC_REG_IN : X_BUF
    port map (
      I => dip6_0_MC_D_99,
      O => NlwBufferSignal_dip6_0_MC_REG_IN
    );
  NlwBufferBlock_dip6_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip6_0_MC_REG_CLK
    );
  NlwBufferBlock_dip6_0_MC_D_IN0 : X_BUF
    port map (
      I => dip6_0_MC_D1_100,
      O => NlwBufferSignal_dip6_0_MC_D_IN0
    );
  NlwBufferBlock_dip6_0_MC_D_IN1 : X_BUF
    port map (
      I => dip6_0_MC_D2_101,
      O => NlwBufferSignal_dip6_0_MC_D_IN1
    );
  NlwBufferBlock_dip6_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip6_0_MC_D1_IN0
    );
  NlwBufferBlock_dip6_0_MC_D1_IN1 : X_BUF
    port map (
      I => dip5_0_MC_UIM_89,
      O => NlwBufferSignal_dip6_0_MC_D1_IN1
    );
  NlwBufferBlock_dip6_1_MC_REG_IN : X_BUF
    port map (
      I => dip6_1_MC_D_103,
      O => NlwBufferSignal_dip6_1_MC_REG_IN
    );
  NlwBufferBlock_dip6_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_dip6_1_MC_REG_CLK
    );
  NlwBufferBlock_dip6_1_MC_D_IN0 : X_BUF
    port map (
      I => dip6_1_MC_D1_104,
      O => NlwBufferSignal_dip6_1_MC_D_IN0
    );
  NlwBufferBlock_dip6_1_MC_D_IN1 : X_BUF
    port map (
      I => dip6_1_MC_D2_105,
      O => NlwBufferSignal_dip6_1_MC_D_IN1
    );
  NlwBufferBlock_dip6_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_dip6_1_MC_D1_IN0
    );
  NlwBufferBlock_dip6_1_MC_D1_IN1 : X_BUF
    port map (
      I => dip5_1_MC_UIM_94,
      O => NlwBufferSignal_dip6_1_MC_D1_IN1
    );
  NlwBufferBlock_mrk_MC_REG_IN : X_BUF
    port map (
      I => mrk_MC_D_108,
      O => NlwBufferSignal_mrk_MC_REG_IN
    );
  NlwBufferBlock_mrk_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_mrk_MC_REG_CLK
    );
  NlwBufferBlock_mrk_MC_D_IN0 : X_BUF
    port map (
      I => mrk_MC_D1_109,
      O => NlwBufferSignal_mrk_MC_D_IN0
    );
  NlwBufferBlock_mrk_MC_D_IN1 : X_BUF
    port map (
      I => mrk_MC_D2_110,
      O => NlwBufferSignal_mrk_MC_D_IN1
    );
  NlwBufferBlock_mrk_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_mrk_MC_D1_IN0
    );
  NlwBufferBlock_mrk_MC_D1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_53,
      O => NlwBufferSignal_mrk_MC_D1_IN1
    );
  NlwBufferBlock_mrk_MC_D1_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_63,
      O => NlwBufferSignal_mrk_MC_D1_IN2
    );
  NlwBufferBlock_mrk_MC_D1_IN3 : X_BUF
    port map (
      I => mrk_MC_UIM_107,
      O => NlwBufferSignal_mrk_MC_D1_IN3
    );
  NlwBufferBlock_mrk_MC_D1_IN4 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd3_111,
      O => NlwBufferSignal_mrk_MC_D1_IN4
    );
  NlwBufferBlock_mrk_MC_D1_IN5 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_112,
      O => NlwBufferSignal_mrk_MC_D1_IN5
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd3_MC_REG_IN : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd3_MC_D_114,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_REG_IN
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_REG_CLK
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd3_MC_D_IN0 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd3_MC_D1_115,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D_IN0
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd3_MC_D_IN1 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd3_MC_D2_116,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D_IN1
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN0
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd3_MC_D1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_53,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN1
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd3_MC_D1_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_63,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd3_MC_D1_IN2
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_REG_IN : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_MC_D_118,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_REG_IN
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_REG_CLK
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D_IN0 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_MC_D1_119,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D_IN0
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D_IN1 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_MC_D2_120,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D_IN1
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_53,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_63,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd3_111,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_53,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_63,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => mrk_MC_UIM_107,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_112,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_7,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => do_0_MC_UIM_53,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => do_1_MC_UIM_63,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd3_111,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_112,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_IN0 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_MC_D2_PT_0_121,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN0
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_IN1 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_122,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN1
    );
  NlwBufferBlock_fsmi_z_cs_FSM_FFd2_MC_D2_IN2 : X_BUF
    port map (
      I => fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_123,
      O => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_IN2
    );
  NlwInverterBlock_mrk_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_mrk_MC_D1_IN1,
      O => NlwInverterSignal_mrk_MC_D1_IN1
    );
  NlwInverterBlock_mrk_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_mrk_MC_D1_IN2,
      O => NlwInverterSignal_mrk_MC_D1_IN2
    );
  NlwInverterBlock_mrk_MC_D1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_mrk_MC_D1_IN3,
      O => NlwInverterSignal_mrk_MC_D1_IN3
    );
  NlwInverterBlock_mrk_MC_D1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_mrk_MC_D1_IN4,
      O => NlwInverterSignal_mrk_MC_D1_IN4
    );
  NlwInverterBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_fsmi_z_cs_FSM_FFd2_MC_D2_PT_2_IN2
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

