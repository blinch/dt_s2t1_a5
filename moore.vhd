-- FSM - moore architecture
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A5


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

architecture moore of fsm is
    signal z_cs:  std_logic_vector (2 downto 0) := "000";
    signal z_ns:  std_logic_vector (2 downto 0);
    signal di_cs:  std_logic_vector (1 downto 0) := "00";
    signal di_ns:  std_logic_vector (1 downto 0);
	signal do_cs:  std_logic_vector (1 downto 0) := "00";
    signal do_ns:  std_logic_vector (1 downto 0);
	signal mrk_cs:  std_logic := '0';
    signal mrk_ns:  std_logic;
    
begin

uesn:
    process (di_cs,z_cs) is 
        variable z_v: std_logic_vector(2 downto 0);
        variable di_v: std_logic_vector(1 downto 0);
    begin
        z_v := z_cs;
        di_v := di_cs;
        
        case z_cs is
            
            when "000" =>       -- state 1
                if (di_v = "11") then
                    z_v := "001";
                else
                    z_v := "000";
                end if;          
            
            
            when "001" =>       -- state 2
                if (di_v = "11") then
                    z_v := "010";
                else
                    z_v := "000";
                end if;
            
            
            when "010" =>       -- state 3            
				if (di_v = "00") then
					z_v := "011";
				elsif (di_v = "11") then
					z_v := "010";
				else
					z_v := "000";
				end if;
            
            
            when "011" =>       -- state 4
				if (di_v = "00") then
					z_v := "100";
				elsif (di_v = "11") then
					z_v := "001";
				else
					z_v := "000";
				end if;
            
            
            when "100" =>       -- state 5
				if (di_v = "11") then
					z_v := "001";
				else
					z_v := "000";
				end if;
            
            
            when others =>
                z_v := "000";
            
        end case;
        
        z_ns <= z_v;
        do_ns <= di_v;
        
    end process uesn;
    


asn:
    process ( z_cs) is
        variable mrk_v : std_logic;
    begin
        if (z_cs = "100")   then
            mrk_v := '1';
        else
            mrk_v := '0';
        end if;
        
        mrk_ns <= mrk_v;
    end process asn;



sequLogic:
    process (clk) is
    begin
        if(clk = '1' and clk'event) then
            if(nres = '0') then
                z_cs  <= (others => '0');
				di_cs <= (others => '0');
				do_cs <= (others => '0');
				mrk_cs <= '0';
            else
                z_cs <= z_ns;
				di_cs <= di_ns;
				do_cs <= do_ns;
				mrk_cs <= mrk_ns;
            end if;
        else
            null;
        end if;
    end process sequLogic;

	di_ns <= di;
	do  <= do_cs;
	mrk <= mrk_cs;

end architecture moore;