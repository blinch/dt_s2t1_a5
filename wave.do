onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb/clk
add wave -noupdate /tb/nres
add wave -noupdate -expand /tb/txData
add wave -noupdate /tb/mrk
add wave -noupdate -expand /tb/do
add wave -noupdate -divider -height 50 {debug shift register}
add wave -noupdate -expand -subitemconfig {/tb/sr6x2_s(0) -expand /tb/sr6x2_s(1) -expand /tb/sr6x2_s(2) -expand /tb/sr6x2_s(3) -expand /tb/sr6x2_s(4) -expand /tb/sr6x2_s(5) -expand} /tb/sr6x2_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1203819 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4095 ns}
