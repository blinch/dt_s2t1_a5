-- 5x2 bit shift register, serial in, parallel out
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A5


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity dbgShfReg is
    port(
        clk     : in  std_logic;
        nres    : in  std_logic;
        dataIn  : in  std_logic_vector (1 downto 0);
        dataOut : out std_logic_vector (1 downto 0)
    );
end dbgShfReg;

architecture arc of dbgShfReg is

signal twobit_ns : std_logic_vector (1 downto 0);
signal twobit_cs : std_logic_vector (1 downto 0) := (others=>'0'); -- startUp init

begin

    shiftreg:
    process(clk)
    begin
        if(clk'event and clk = '1') then
            if(nres ='0') then
                twobit_cs <= (others=>'0');
            else     
                twobit_cs <= twobit_ns;
            end if;
         end if;
    end process shiftreg;
    
    twobit_ns <= dataIn;
    dataOut <= twobit_cs;

end arc;