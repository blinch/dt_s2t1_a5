-- FSM - finite state machine
-- by Torsten Huhn and Ulrich ter Horst
-- Dec 2015
-- DTP_A5


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;

entity fsm is
    port (
        clk  : in std_logic;                       -- CLocK
        nres : in std_logic;                       -- Not RESet, low active reset
        di   : in  std_logic_vector( 1 downto 0 ); -- Data In
        do   : out std_logic_vector( 1 downto 0 ); -- Data Out
        mrk  : out std_logic                       -- MaRK off 1st data following pattern
    );
end entity fsm;